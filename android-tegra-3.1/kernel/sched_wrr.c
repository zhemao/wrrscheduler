static const struct sched_class sched_wrr_class;

#define WRR_DEFAULT_WEIGHT 10

static inline unsigned int weight_to_ts(unsigned int weight)
{
	return (weight * HZ) / 100;
}

static inline struct task_struct *wrr_task_of(struct sched_wrr_entity *wrr_se)
{
	return container_of(wrr_se, struct task_struct, wrr);
}

static inline struct rq *rq_of_wrr_rq(struct wrr_rq *wrr_rq)
{
	return container_of(wrr_rq, struct rq, wrr);
}

static inline struct wrr_rq *wrr_rq_of_se(struct sched_wrr_entity *wrr_se)
{
	struct task_struct *p = wrr_task_of(wrr_se);
	struct rq *rq = task_rq(p);

	return &rq->wrr;
}

static inline int on_wrr_rq(struct sched_wrr_entity *wrr_se)
{
	return !list_empty(&wrr_se->run_list);
}

static void 
wrr_set_task_weight(struct rq *rq, struct task_struct *p, unsigned int weight)
{
	unsigned int oldweight = p->wrr.weight;
	int weightdiff = weight - oldweight;
	
	p->wrr.weight = weight;
	rq->wrr.total_weight += weightdiff;
}

static void enqueue_task_wrr(struct rq *rq, struct task_struct *p, int flags)
{
	struct wrr_rq *wrr_rq = &rq->wrr;
	struct sched_wrr_entity *wrr_se = &p->wrr;

	list_add_tail(&wrr_se->run_list, &wrr_rq->queue);

	wrr_rq->total_weight += wrr_se->weight;

	if (!task_current(rq, p))
		wrr_se->movable = 1;
}

static void dequeue_task_wrr(struct rq *rq, struct task_struct *p, int flags)
{
	struct wrr_rq *wrr_rq = &rq->wrr;
	struct sched_wrr_entity *wrr_se = &p->wrr;

	list_del_init(&wrr_se->run_list);

	wrr_rq->total_weight -= wrr_se->weight;
}

static void requeue_task_wrr(struct rq *rq, struct task_struct *p)
{
	struct wrr_rq *wrr_rq = &rq->wrr;
	struct sched_wrr_entity *wrr_se = &p->wrr;

	list_move_tail(&wrr_se->run_list, &wrr_rq->queue);
}

static void yield_task_wrr(struct rq *rq)
{
	requeue_task_wrr(rq, rq->curr);
}

static struct task_struct *pick_next_task_wrr(struct rq *rq)
{
	struct wrr_rq *wrr_rq = &rq->wrr;
	struct sched_wrr_entity *next;
	struct task_struct *p;
	struct list_head *list;
	int entity_count = 0;

	if (list_empty(&wrr_rq->queue))
		return NULL;

	list_for_each(list, &wrr_rq->queue) {
		entity_count++;
	}

	pr_info("%d items in queue\n", entity_count);

	next = list_entry(wrr_rq->queue.next,
			  struct sched_wrr_entity,
			  run_list);

	next->time_slice = weight_to_ts(next->weight);
	next->movable = 0;

	p = wrr_task_of(next);

	pr_info("running %d\n", p->pid);

	return p;
}

static void put_prev_task_wrr(struct rq *rq, struct task_struct *p)
{
	struct sched_wrr_entity *wrr_se = &p->wrr;

	if (on_wrr_rq(wrr_se))
		wrr_se->movable = 1;
}

static void
check_preempt_curr_wrr(struct rq *rq, struct task_struct *p, int flags)
{
}

static void set_curr_task_wrr(struct rq *rq)
{
	struct sched_wrr_entity *wrr_se = &rq->curr->wrr;

	wrr_se->movable = 0;
}

static void task_tick_wrr(struct rq *rq, struct task_struct *p, int queued)
{
	pr_info("%d has %d ticks left\n", p->pid, p->wrr.time_slice);

	if (--p->wrr.time_slice)
		return;

	pr_info("time slice of %d has expired\n", p->pid);
	
	p->wrr.time_slice = weight_to_ts(p->wrr.weight);

	if (p->wrr.run_list.prev != p->wrr.run_list.next) {
		pr_info("%d is being rescheduled\n", p->pid);
		requeue_task_wrr(rq, p);
		set_tsk_need_resched(p);
	}
}

static unsigned int
get_rr_interval_wrr(struct rq *rq, struct task_struct *task)
{
	struct sched_wrr_entity *wrr_se = &task->wrr;

	return weight_to_ts(wrr_se->weight);
}

static void prio_changed_wrr(struct rq *rq, struct task_struct *p, int oldprio)
{
}

static void switched_to_wrr(struct rq *rq, struct task_struct *p)
{
	pr_info("switched to WRR scheduler\n");

	if (p->on_rq)
		wrr_set_task_weight(rq, p, WRR_DEFAULT_WEIGHT);
}

#ifdef CONFIG_SMP

static void rq_online_wrr(struct rq *rq)
{
	/* start_wrr_lb_timer(&def_wrr_bandwidth); */
}

static void wrr_load_balance(void)
{

}

static int select_task_rq_wrr(struct task_struct *p, int sd_flags, int flags)
{
	unsigned int cpu, lowest_cpu;
	unsigned int lowest_weight;
	struct rq *rq;

	lowest_cpu = task_cpu(p);
	rq = task_rq(p);
	lowest_weight = rq->wrr.total_weight;

	for_each_cpu(cpu, cpu_online_mask) {
		rq = cpu_rq(cpu);

		pr_info("cpu %u has weight %lu\n", cpu, rq->wrr.total_weight);

		if (rq->wrr.total_weight < lowest_weight) {
			lowest_cpu = cpu;
			lowest_weight = rq->wrr.total_weight;
		}
	}

	pr_info("picked cpu %u for task %d\n", lowest_cpu, p->pid);

	return lowest_cpu;
}

#endif

static const struct sched_class sched_wrr_class = {
	.next			= &fair_sched_class,
	.enqueue_task		= enqueue_task_wrr,
	.dequeue_task		= dequeue_task_wrr,
	.yield_task		= yield_task_wrr,

	.check_preempt_curr	= check_preempt_curr_wrr,

	.pick_next_task		= pick_next_task_wrr,
	.put_prev_task		= put_prev_task_wrr,

	#ifdef CONFIG_SMP
	.select_task_rq		= select_task_rq_wrr,
	.rq_online              = rq_online_wrr,
	#endif

	.set_curr_task		= set_curr_task_wrr,
	.task_tick		= task_tick_wrr,

	.get_rr_interval	= get_rr_interval_wrr,

	.prio_changed		= prio_changed_wrr,
	.switched_to		= switched_to_wrr
};
