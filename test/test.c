#define _GNU_SOURCE
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <ctype.h>
#include <unistd.h>
#include <math.h>
#include <string.h>
#include <gmp.h>
#include <time.h>
#include <linux/unistd.h>
#include <sched.h>
#include "prime.h"

#define set_weight(pid, weight) syscall(376, (pid), (weight))
#define get_weight(pid) syscall(377, (pid))
#define valid_weight(w) ((w) >= 1 && (w) <= 20)

static void find_factors(mpz_t base);

static int isnumeric(const char *arg)
{
	unsigned int i;
	for (i = 0; i < strlen(arg); i++)
		if (!isdigit(arg[i]))
			return 0;
	return 1;
}

int main(int argc, const char *argv[])
{
	mpz_t largenum;
	int weight_query;
	int weight;
	int start, end;
	struct sched_param param = { 0 };

	if (argc < 3) {
		printf("Usage: %s <number> <weight>\n", argv[0]);
		return EXIT_FAILURE;
	}

	if (mpz_init_set_str(largenum, argv[1], 10) < 0) {
		perror("Invalid number");
		return EXIT_FAILURE;
	}

	/* TODO: Handle weight argument here */

	if (!isnumeric(argv[2])) {
		perror("Invalid number");
		return EXIT_FAILURE;
	}
	weight = atoi(argv[2]);
	if (!valid_weight(weight)) {
		perror("Invalid weight");
		return EXIT_FAILURE;
	}

	printf("attempting to switch to WRR\n");

	if (sched_setscheduler(getpid(), 6, &param) < 0) {
		perror("sched_setscheduler()");
		return EXIT_FAILURE;
	}

	weight_query = get_weight(0);
	if (weight_query < 0) {
		perror("get old weight");
		return EXIT_FAILURE;
	}
	printf("Old weight:\t%i\n", weight_query);

	if (set_weight(0, weight) != 0) {
		perror("set new weight");
		return EXIT_FAILURE;
	}
	printf("Set weight:\t%i\n", weight);

	weight_query = get_weight(0);
	if (weight_query < 0) {
		perror("get new weight");
		return EXIT_FAILURE;
	}
	printf("New weight:\t%i\n", weight_query);

	start = time(0);

	find_factors(largenum);

	end = time(0);

	printf("Runtime:\t%d s\n", end - start);

	return EXIT_SUCCESS;
}

/*
 * DO NOT MODIFY BELOW THIS LINE
 * -----------------------------
 */

static void find_factors(mpz_t base)
{
	char *str;
	int res;
	mpz_t i;
	mpz_t half;
	mpz_t two;

	mpz_init_set_str(two, "2", 10);
	mpz_init_set_str(i, "2", 10);
	mpz_init(half);
	mpz_cdiv_q(half, base, two);

	str = mpz_to_str(base);
	if (!str)
		return;

	/*
	 * We simply return the prime number itself if the base is prime.
	 * (We use the GMP probabilistic function with 10 repetitions).
	 */
	res = mpz_probab_prime_p(base, 10);
	if (res) {
		printf("%s is a prime number\n", str);
		free(str);
		return;
	}

	printf("Trial: prime factors for %s are:", str);
	free(str);
	do {
		if (mpz_divisible_p(base, i) && verify_is_prime(i)) {
			str = mpz_to_str(i);
			if (!str)
				return;
			printf(" %s", str);
			free(str);
		}

		mpz_nextprime(i, i);
	} while (mpz_cmp(i, half) <= 0);
	printf("\n");
}
