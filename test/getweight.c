#include <unistd.h>
#include <sys/syscall.h>

#include <stdlib.h>
#include <stdio.h>

int main(int argc, char *argv[])
{
	int pid;
	int weight;

	if (argc < 2)
		pid = 0;
	else
		pid = atoi(argv[1]);
	
	weight = syscall(377, pid);

	if (weight < 0)
		return weight;

	printf("%d\n", weight);

	return 0;
}
