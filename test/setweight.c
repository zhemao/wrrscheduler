#include <unistd.h>
#include <sys/syscall.h>

#include <stdlib.h>
#include <stdio.h>

int main(int argc, char *argv[])
{
	int pid;
	int weight;

	if (argc < 3) {
		fprintf(stderr, "Usage: %s pid weight\n", argv[0]);
		exit(EXIT_FAILURE);
	} else {
		pid = atoi(argv[1]);
		weight = atoi(argv[2]);
	}
	
	if (syscall(376, pid, weight) < 0) {
		perror("error");
		return EXIT_FAILURE;
	}

	return 0;
}
