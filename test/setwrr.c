#include <sched.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
	int pid;
	struct sched_param param = { 0 };

	if (argc < 2) {
		fprintf(stderr, "must supply pid\n");
		exit(EXIT_FAILURE);
	}

	pid = atoi(argv[1]);

	if (pid <= 0) {
		fprintf(stderr, "invalid pid\n");
		exit(EXIT_FAILURE);
	}

	if (sched_setscheduler(pid, 6, &param) != 0) {
		perror("error");
		return EXIT_FAILURE;
	}

	return 0;
}
